/* This is free and unencumbered software released into the public domain. */

#ifndef CLIXX_EXTERN_H
#define CLIXX_EXTERN_H

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* CLIXX_EXTERN_H */
